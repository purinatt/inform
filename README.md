กลุ่ม Lost & Found 
จีระวัฒน์ หนักแน่น 5710450031
ธีระพล ไวยนิยี 5810613413
ศิริรัตน์ บุณยัษเฐียร 5910490035
ภูริณัฐ ธรรมราช 5910613347

Heroku:  https://peaceful-falls-74616.herokuapp.com/

Test:
    เตรียมข้อมูล:
        $ cd testproject
        $ bundle install --withput production
        $ rails db:migrate RAILS_ENV=test
    ทดสอบ Story (Request Test):
        $ rspec spec/requests/signups_spec.rb
        $ rspec spec/requests/logins_spec.rb 
        
        
    ทดสอบ Model:
        $ rspec spec/model/user_spec.rb
    ทดสอบ Controller:
        $ rspec spec/controllers/static_pages_controller_spec.rb
        $ rspec spec/controllers/users_controller_spec.rb
        $ rspec spec/controllers/sessions_controller.rb

   