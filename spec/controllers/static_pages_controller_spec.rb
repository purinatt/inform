require 'rails_helper'

describe StaticPagesController  do 
    
    describe 'about' do
        it 'check view about in controller' do
            get :about
            expect(response).to render_template('about')
        end
    end
    describe 'found' do
        it 'check view found in controller' do
            get :found
            expect(response).to render_template('found')
        end
    end
  describe 'lost' do
        it 'check view lost in controller' do
            get :lost
            expect(response).to render_template('lost')
        end
    end
end