require 'rails_helper'
describe "login",type: :request do
  fixtures :users
  before :each do
    @user_test = users(:anna)
  end
  it "with invalid information" do
    get login_path
    expect(response).to render_template 'sessions/new'
    post login_path, params: { session: { username: "", password: "" } }
    expect(response).to render_template 'sessions/new'
    expect(flash).not_to be_empty
    
  end
  
  it "with valid information" do
    get login_path
    post login_path, params: { session: { username: @user_test.username,
                                          password: "12345678"} }
    expect(response).to redirect_to(@user_test)
    
    
  end
end
