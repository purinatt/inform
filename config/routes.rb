Rails.application.routes.draw do
  resources :users
  get    '/about',  to: 'static_pages#about'
  get    '/found',  to: 'static_pages#found'
  get    '/lost',  to: 'static_pages#lost'
  get    '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  get    '/inform',  to: 'inform#new'
  post    '/inform', to:  'inform#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  root 'static_pages#lost'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
