class Inform < ApplicationRecord
validates :username,  presence: true, length: { maximum: 40 } , uniqueness: { case_sensitive: false }
validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
validates :contact,  presence: true, length: { maximum: 255 }
validates :tel_number,  presence: true, length: { is: 10 } , numericality: { only_integer: true }
end